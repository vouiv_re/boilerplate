# Boilerplate

This is my usual work's stack for front project.

* Lite-server
* SASS
* concurrently

***

```sh
npm install
npm run all
```

It will launch:

* A sass watcher
* A lite server

To minify the SASS launch:

```sh
npm run sass-minify
```
